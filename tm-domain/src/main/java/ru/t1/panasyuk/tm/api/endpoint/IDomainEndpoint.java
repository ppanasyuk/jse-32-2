package ru.t1.panasyuk.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.dto.request.data.*;
import ru.t1.panasyuk.tm.dto.response.data.*;

public interface IDomainEndpoint {

    @NotNull
    DataBackupLoadResponse loadDataBackup(@NotNull DataBackupLoadRequest request);

    @NotNull
    DataBackupSaveResponse saveDataBackup(@NotNull DataBackupSaveRequest request);

    @NotNull
    DataBase64LoadResponse loadDataBase64(@NotNull DataBase64LoadRequest request);

    @NotNull
    DataBase64SaveResponse saveDataBase64(@NotNull DataBase64SaveRequest request);

    @NotNull
    DataBinaryLoadResponse loadDataBinary(@NotNull DataBinaryLoadRequest request);

    @NotNull
    DataBinarySaveResponse saveDataBinary(@NotNull DataBinarySaveRequest request);

    @NotNull
    DataJsonLoadFasterXMLResponse loadDataJsonFasterXML(@NotNull DataJsonLoadFasterXMLRequest request);

    @NotNull
    DataJsonLoadJaxBResponse loadDataJsonJaxB(@NotNull DataJsonLoadJaxBRequest request);

    @NotNull
    DataJsonSaveFasterXMLResponse saveDataJsonFasterXML(@NotNull DataJsonSaveFasterXMLRequest request);

    @NotNull
    DataJsonSaveJaxBResponse saveDataJsonJaxB(@NotNull DataJsonSaveJaxBRequest request);

    @NotNull
    DataXMLLoadFasterXMLResponse loadDataXMLFasterXML(@NotNull DataXMLLoadFasterXMLRequest request);

    @NotNull
    DataXMLLoadJaxBResponse loadDataXMLJaxB(@NotNull DataXMLLoadJaxBRequest request);

    @NotNull
    DataXMLSaveFasterXMLResponse saveDataXMLFasterXML(@NotNull DataXMLSaveFasterXMLRequest request);

    @NotNull
    DataXMLSaveJaxBResponse saveDataXMLJaxB(@NotNull DataXMLSaveJaxBRequest request);

    @NotNull
    DataYamlLoadFasterXMLResponse loadDataYamlFasterXML(@NotNull DataYamlLoadFasterXMLRequest request);

    @NotNull
    DataYamlSaveFasterXMLResponse saveDataYamlFasterXML(@NotNull DataYamlSaveFasterXMLRequest request);

}