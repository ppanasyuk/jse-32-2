package ru.t1.panasyuk.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.dto.request.user.UserLoginRequest;
import ru.t1.panasyuk.tm.dto.request.user.UserLogoutRequest;
import ru.t1.panasyuk.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.panasyuk.tm.dto.response.user.UserLoginResponse;
import ru.t1.panasyuk.tm.dto.response.user.UserLogoutResponse;
import ru.t1.panasyuk.tm.dto.response.user.UserViewProfileResponse;

public interface IAuthEndpoint {

    @NotNull
    UserLoginResponse loginUser(@NotNull UserLoginRequest request);

    @NotNull
    UserLogoutResponse logoutUser(@NotNull UserLogoutRequest request);

    @NotNull
    UserViewProfileResponse viewUserProfile(@NotNull UserViewProfileRequest request);

}