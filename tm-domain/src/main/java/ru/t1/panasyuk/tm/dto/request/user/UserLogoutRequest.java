package ru.t1.panasyuk.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.panasyuk.tm.dto.request.AbstractRequest;

@Getter
@Setter
@NoArgsConstructor
public final class UserLogoutRequest extends AbstractRequest {
}