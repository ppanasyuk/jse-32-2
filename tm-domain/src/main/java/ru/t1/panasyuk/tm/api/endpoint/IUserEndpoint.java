package ru.t1.panasyuk.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.dto.request.user.*;
import ru.t1.panasyuk.tm.dto.response.user.*;

public interface IUserEndpoint {

    @NotNull
    UserChangePasswordResponse changeUserPassword(@NotNull UserChangePasswordRequest request);

    @NotNull
    UserLockResponse lockUser(@NotNull UserLockRequest request);

    @NotNull
    UserRegistryResponse registryUser(@NotNull UserRegistryRequest request);

    @NotNull
    UserRemoveResponse removeUser(@NotNull UserRemoveRequest request);

    @NotNull
    UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request);

    @NotNull
    UserUpdateProfileResponse updateUserProfile(@NotNull UserUpdateProfileRequest request);

}