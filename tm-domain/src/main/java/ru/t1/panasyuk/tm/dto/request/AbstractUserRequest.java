package ru.t1.panasyuk.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public abstract class AbstractUserRequest extends AbstractRequest {

    @NotNull
    private String userId;

}