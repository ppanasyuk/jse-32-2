package ru.t1.panasyuk.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.api.endpoint.IDomainEndpoint;
import ru.t1.panasyuk.tm.api.service.IServiceLocator;
import ru.t1.panasyuk.tm.dto.request.data.*;
import ru.t1.panasyuk.tm.dto.response.data.*;
import ru.t1.panasyuk.tm.enumerated.Role;

public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public DataBackupLoadResponse loadDataBackup(@NotNull final DataBackupLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBackup();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    public DataBackupSaveResponse saveDataBackup(@NotNull final DataBackupSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBackup();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    public DataBase64LoadResponse loadDataBase64(@NotNull final DataBase64LoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBase64();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    public DataBase64SaveResponse saveDataBase64(@NotNull final DataBase64SaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBase64();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    public DataBinaryLoadResponse loadDataBinary(@NotNull final DataBinaryLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBinary();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    public DataBinarySaveResponse saveDataBinary(@NotNull final DataBinarySaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBinary();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    public DataJsonLoadFasterXMLResponse loadDataJsonFasterXML(@NotNull final DataJsonLoadFasterXMLRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataJsonFasterXML();
        return new DataJsonLoadFasterXMLResponse();
    }

    @NotNull
    @Override
    public DataJsonLoadJaxBResponse loadDataJsonJaxB(@NotNull final DataJsonLoadJaxBRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataJsonJaxB();
        return new DataJsonLoadJaxBResponse();
    }

    @NotNull
    @Override
    public DataJsonSaveFasterXMLResponse saveDataJsonFasterXML(@NotNull final DataJsonSaveFasterXMLRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataJsonFasterXML();
        return new DataJsonSaveFasterXMLResponse();
    }

    @NotNull
    @Override
    public DataJsonSaveJaxBResponse saveDataJsonJaxB(@NotNull final DataJsonSaveJaxBRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataJsonJaxB();
        return new DataJsonSaveJaxBResponse();
    }

    @NotNull
    @Override
    public DataXMLLoadFasterXMLResponse loadDataXMLFasterXML(@NotNull final DataXMLLoadFasterXMLRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataXMLFasterXML();
        return new DataXMLLoadFasterXMLResponse();
    }

    @NotNull
    @Override
    public DataXMLLoadJaxBResponse loadDataXMLJaxB(@NotNull final DataXMLLoadJaxBRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataXMLJaxB();
        return new DataXMLLoadJaxBResponse();
    }

    @NotNull
    @Override
    public DataXMLSaveFasterXMLResponse saveDataXMLFasterXML(@NotNull final DataXMLSaveFasterXMLRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataXMLFasterXML();
        return new DataXMLSaveFasterXMLResponse();
    }

    @NotNull
    @Override
    public DataXMLSaveJaxBResponse saveDataXMLJaxB(@NotNull final DataXMLSaveJaxBRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataXMLJaxB();
        return new DataXMLSaveJaxBResponse();
    }

    @NotNull
    @Override
    public DataYamlLoadFasterXMLResponse loadDataYamlFasterXML(@NotNull final DataYamlLoadFasterXMLRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataYamlFasterXML();
        return new DataYamlLoadFasterXMLResponse();
    }

    @NotNull
    @Override
    public DataYamlSaveFasterXMLResponse saveDataYamlFasterXML(@NotNull final DataYamlSaveFasterXMLRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataYamlFasterXML();
        return new DataYamlSaveFasterXMLResponse();
    }

}
