package ru.t1.panasyuk.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.api.endpoint.ISystemEndpointClient;
import ru.t1.panasyuk.tm.dto.request.system.ServerAboutRequest;
import ru.t1.panasyuk.tm.dto.request.system.ServerVersionRequest;
import ru.t1.panasyuk.tm.dto.response.system.ServerAboutResponse;
import ru.t1.panasyuk.tm.dto.response.system.ServerVersionResponse;

public final class SystemEndpointClient extends AbstractClient implements ISystemEndpointClient {

    @NotNull
    @Override
    @SneakyThrows
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        return call(request, ServerAboutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        return call(request, ServerVersionResponse.class);
    }

}