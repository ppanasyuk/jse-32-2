package ru.t1.panasyuk.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.api.endpoint.IDomainEndpointClient;
import ru.t1.panasyuk.tm.dto.request.data.*;
import ru.t1.panasyuk.tm.dto.request.user.UserLoginRequest;
import ru.t1.panasyuk.tm.dto.request.user.UserLogoutRequest;
import ru.t1.panasyuk.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.panasyuk.tm.dto.response.data.*;

@NoArgsConstructor
public final class DomainEndpointClient extends AbstractClient implements IDomainEndpointClient {

    public DomainEndpointClient(@NotNull final AbstractClient client) {
        super(client);
    }

    @NotNull
    @Override
    public DataBackupLoadResponse loadDataBackup(@NotNull final DataBackupLoadRequest request) {
        return call(request, DataBackupLoadResponse.class);
    }

    @NotNull
    @Override
    public DataBackupSaveResponse saveDataBackup(@NotNull final DataBackupSaveRequest request) {
        return call(request, DataBackupSaveResponse.class);
    }

    @NotNull
    @Override
    public DataBase64LoadResponse loadDataBase64(@NotNull final DataBase64LoadRequest request) {
        return call(request, DataBase64LoadResponse.class);
    }

    @NotNull
    @Override
    public DataBase64SaveResponse saveDataBase64(@NotNull final DataBase64SaveRequest request) {
        return call(request, DataBase64SaveResponse.class);
    }

    @NotNull
    @Override
    public DataBinaryLoadResponse loadDataBinary(@NotNull final DataBinaryLoadRequest request) {
        return call(request, DataBinaryLoadResponse.class);
    }

    @NotNull
    @Override
    public DataBinarySaveResponse saveDataBinary(@NotNull final DataBinarySaveRequest request) {
        return call(request, DataBinarySaveResponse.class);
    }

    @NotNull
    @Override
    public DataJsonLoadFasterXMLResponse loadDataJsonFasterXML(@NotNull final DataJsonLoadFasterXMLRequest request) {
        return call(request, DataJsonLoadFasterXMLResponse.class);
    }

    @NotNull
    @Override
    public DataJsonLoadJaxBResponse loadDataJsonJaxB(@NotNull final DataJsonLoadJaxBRequest request) {
        return call(request, DataJsonLoadJaxBResponse.class);
    }

    @NotNull
    @Override
    public DataJsonSaveFasterXMLResponse saveDataJsonFasterXML(@NotNull final DataJsonSaveFasterXMLRequest request) {
        return call(request, DataJsonSaveFasterXMLResponse.class);
    }

    @NotNull
    @Override
    public DataJsonSaveJaxBResponse saveDataJsonJaxB(@NotNull final DataJsonSaveJaxBRequest request) {
        return call(request, DataJsonSaveJaxBResponse.class);
    }

    @NotNull
    @Override
    public DataXMLLoadFasterXMLResponse loadDataXMLFasterXML(@NotNull final DataXMLLoadFasterXMLRequest request) {
        return call(request, DataXMLLoadFasterXMLResponse.class);
    }

    @NotNull
    @Override
    public DataXMLLoadJaxBResponse loadDataXMLJaxB(@NotNull final DataXMLLoadJaxBRequest request) {
        return call(request, DataXMLLoadJaxBResponse.class);
    }

    @NotNull
    @Override
    public DataXMLSaveFasterXMLResponse saveDataXMLFasterXML(@NotNull final DataXMLSaveFasterXMLRequest request) {
        return call(request, DataXMLSaveFasterXMLResponse.class);
    }

    @NotNull
    @Override
    public DataXMLSaveJaxBResponse saveDataXMLJaxB(@NotNull final DataXMLSaveJaxBRequest request) {
        return call(request, DataXMLSaveJaxBResponse.class);
    }

    @NotNull
    @Override
    public DataYamlLoadFasterXMLResponse loadDataYamlFasterXML(@NotNull final DataYamlLoadFasterXMLRequest request) {
        return call(request, DataYamlLoadFasterXMLResponse.class);
    }

    @NotNull
    @Override
    public DataYamlSaveFasterXMLResponse saveDataYamlFasterXML(@NotNull final DataYamlSaveFasterXMLRequest request) {
        return call(request, DataYamlSaveFasterXMLResponse.class);
    }

}