package ru.t1.panasyuk.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.endpoint.IAuthEndpointClient;
import ru.t1.panasyuk.tm.api.endpoint.IUserEndpointClient;
import ru.t1.panasyuk.tm.command.AbstractCommand;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.exception.entity.UserNotFoundException;
import ru.t1.panasyuk.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected IUserEndpointClient getUserEndpointClient() {
        return getServiceLocator().getUserEndpointClient();
    }

    @NotNull
    protected IAuthEndpointClient getAuthEndpointClient() {
        return getServiceLocator().getAuthEndpointClient();
    }

    protected void showUser(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}