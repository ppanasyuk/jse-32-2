package ru.t1.panasyuk.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;

import java.net.Socket;

public interface ISystemEndpointClient extends ISystemEndpoint {

    void setSocket(@NotNull Socket socket);

}