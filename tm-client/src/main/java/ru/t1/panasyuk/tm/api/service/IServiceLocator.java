package ru.t1.panasyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IAuthEndpointClient getAuthEndpointClient();

    @NotNull
    IEndpointClient getConnectionEndpointClient();

    @NotNull
    IDomainEndpointClient getDomainEndpointClient();

    @NotNull
    IProjectEndpointClient getProjectEndpointClient();

    @NotNull
    ISystemEndpointClient getSystemEndpointClient();

    @NotNull
    ITaskEndpointClient getTaskEndpointClient();

    @NotNull
    IUserEndpointClient getUserEndpointClient();

}